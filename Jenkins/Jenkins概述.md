# Jenkins

问题的三要素？

什么是Jenkins？

Jenkins可以干什么？解决了哪些问题

怎么使用Jenkins

## 1.Jenkins概述

Jenkins是Java语言开发的一款开源 CI&CD 软件，用于自动化各种任务，包
括构建、测试和部署软件。
jenkins支持各种运行方式，可通过系统包、Docker 或者通过一个独立的
Java 程序。
Jenkins分布式构建支持能够让多台计算机一起构建/测试。
Jenkins文件识别能够跟踪哪次构建生成哪些 jar，哪次构建使用哪个版本
的 jar 等。
Jenkins丰富的插件支持，支持扩展插件，你可以开发适合自己团队使用的
工具，如 git、svn、maven、docker 等。
Jenkins官网地址：https://www.jenkins.io/

![Jenkins_logo](D:\桌面\Git本地仓库\Jenkins\img\Jenkins_logo.png)

持续集成流程说明：

![Jenkins架构图.png](D:\桌面\Git本地仓库\Jenkins\img\架构图.png)

- 首先，开发人员每天进行代码提交，提交到 Git 仓库。
- 然后，Jenkins 作为持续集成工具，使用 Git 工具到 Git 仓库拉取代码
  到集成服务器，再配合 JDK，Maven 等软件完成代码编译，代码测试
  与审查，测试，打包等工作，在这个过程中每一步出错，都重新再执行
  一次整个流程。
- 最后，Jenkins 把生成的 jar 或 war 包分发到测试服务器或者生产服务
  器，测试人员或用户就可以访问应用。