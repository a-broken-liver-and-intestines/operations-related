# Git-Git Lab-Jenkins

👋 Welcome to Notion!

$$
\Large\textsf{自动化运维平台}
$$

# 第一节：DevOps概念介绍

DevOps是一种重视开发(Dev)和运维(Ops)之间沟通合作的文化、运动和实践。以下是DevOps的几个基本概念:

1. 自动化 - 将编译、测试、发布等流程自动化,使交付更轻松可靠。利用CI/CD等实践。
2. 持续集成 - 将代码集成到主分支上的频率更高,每天多次提交。快速发现和定位问题。
3. 持续交付/部署 - 将软件更频繁地、持续地交付给质量团队或用户,并能迅速部署到生产环境。
4. 基础设施即代码 - 将基础设施用代码方式实现,可以像应用程序代码一样进行版本管理等。
5. 监控和日志 - 扩展监控,从开发到运维全流程数据收集,并利用日志分析。
6. 敏捷思想 - 借鉴敏捷开发理念,提高团队的敏捷性。

综合来说,DevOps强调开发和运维之间的协作,通过自动化、监控等手段提高交付速度和质量。

## 1.DevOps起源

一款产品从零开始到最终交付大概包括如下几个阶段：规划、编码、构
建、测试、发布、部署、维护。

![Untitled](Git-Git%20Lab-Jenkins%2046657a7a4e974cc1b9ae9a6039d8b09c/Untitled.png)

最初，程序比较简单，工作量不大，程序员一个人可以完成所有阶段的工
作，但是随着软件产业的日益壮大，软件的功能也在不断的增加，随之而
来的就是复杂度不断的攀升，导致一个程序员无法在有效的时间完成所有
的工作，随后开始精细化分工。

![Untitled](Git-Git%20Lab-Jenkins%2046657a7a4e974cc1b9ae9a6039d8b09c/Untitled%201.png)

分工之后，开发人员完成编码工作之后，将代码交给QA（质量保障）团队
进行测试，然后将最终的发布版交给运维团队去部署到生产环境对用户提
供服务，既开发、测试、部署。

但是早期的软件交付被人们称为瀑布模型。瀑布模型就是等一个阶段所有
工作完成之后，才能进入下一个阶段。

![Untitled](Git-Git%20Lab-Jenkins%2046657a7a4e974cc1b9ae9a6039d8b09c/Untitled%202.png)

这种模型适合条件比较理想化（用户需求非常明确、开发时间非常充足）
的项目，大家按部就班的执行自己本质工作即可，但是随着用户的需求不
断增加，软件市场的竞争变得越加的激烈，根本无法实现理想化的交付，
在这个情况下，大家发现，笨重迟缓的瀑布式开发已经不合时宜，于是软
件开发团队引入了一个新的概念，那就是大名鼎鼎的——**敏捷开发**。

![Untitled](Git-Git%20Lab-Jenkins%2046657a7a4e974cc1b9ae9a6039d8b09c/Untitled%203.png)

**敏捷开发可以大幅的提高开发团队的工作效率，让版本更新的速度变得更
块，更快的发现问题，产品更快的交付到用户手中，团队也可以快速得到
用户的反馈，从而进行快速的响应。**

## CI/CD概念 （持续集成/持续交付）

### CI（持续集成）

持续集成（Continuous Integration，简称CI）指的是频繁地（一天多次）
将代码集成到主干。
持续集成的目的就是让产品可以快速迭代，同时还能保证高质量，它的核
心措施是将代码集成到主干之前，必须通过自动化测试，只要有一个测试
用例失败，就不能集成。
通过持续集成，团队可以实现敏捷开发（速度快，效率高），简而言之，
敏捷开发很大一部分要归功于持续集成。

![Untitled](Git-Git%20Lab-Jenkins%2046657a7a4e974cc1b9ae9a6039d8b09c/Untitled%204.png)

### CD（持续交付）

而CD则对应多个英文Continuous Delivery（持续交付）或者Continuous
Deployment（持续部署）。

持续交付在持续集成的基础上，将集成后的代码部署到更贴近真实运行环
境的「类生产环境」中再次进行测试。如果代码没有问题，可以继续手动
部署到生产环境中。

![Untitled](Git-Git%20Lab-Jenkins%2046657a7a4e974cc1b9ae9a6039d8b09c/Untitled%205.png)

**持续部署则是在持续交付的基础上，把部署到生产环境的过程自动化。**

# 第二节：常见代码部署方式

## 1.蓝绿部署介绍

两个完全一样的生产环境配置部署的两套产品，一个是当前正在运行的生
产环境，它接收所有用户流量(蓝色)。另一个是它的克隆，但是是空闲的
(绿色)。两者使用的配置，数据库，主机等全部一致

**具体过程**

1. 当前版本业务正常访问 (蓝色) ；
2. 另一套环境部署新代码 (绿色) ，代码可能是增加功能或修复 BUG ；
3. 测试通过后将用户请求流量全部切到新版本 (绿色) 环境；
4. 观察测试，若新版本 (绿色) 稳定无异常，表示本次蓝绿发布成功；
5. 观察测试，若有异常直接切换旧版本（蓝色）；
6. 下次升级，将旧版本 (蓝色) 所在环境升级到新版本，继续进行蓝绿发
布，其特点是业务无中断，升级风险几乎为零；

![Untitled](Git-Git%20Lab-Jenkins%2046657a7a4e974cc1b9ae9a6039d8b09c/Untitled%206.png)

**适用场景：**

- 不停止 老版本，另部署一套新版本；测试完成之后，删除老版本；
- 蓝绿发布是一种用于升级与更新的发布策略，部署的最小维度是容器，而发布最小的维度是应用
- 蓝绿发布对于增量升级有比较好的支持，但是对于涉及数据表结构变更
等等不可逆转的升级，并不完全合适用蓝绿发布来实现，需要结合一些
业务的逻辑以及数据迁移与回滚的策略才可以完全满足需求。

## 金丝雀发布介绍

金丝雀发布也叫灰度发布，是指在灰与白之间，能够平滑过渡的一种发布
方式，灰度发布是**增量发布**的一种类型；灰度发布是在原有版本可用的情
况下，同时部署一些新版本应用作为 “金丝雀(小白鼠)” 进行测试新版本的
性能与表现等，以保障整个系统稳定持续正常运行的情况下，尽早发现、
调整问题。

![Untitled](Git-Git%20Lab-Jenkins%2046657a7a4e974cc1b9ae9a6039d8b09c/Untitled%207.png)

**具体过程**

1. 准备好部署各个阶段的工件，包括：构建工件，测试脚本，配置文件和
部署清单文件；
2. 从负载均衡列表中移除掉 “金丝雀” 服务器；
3. 升级 “金丝雀” 应用（排掉原有流量并进行部署）；
4. 对应用进行自动化测试；
5. 将 “金丝雀” 服务器重新添加到负载均衡列表中（连通性和健康检
查）；
6. 如果 “金丝雀” 在线使用测试成功，升级剩余的其他服务器（否则就回
滚）。

**适用场景:**

- 不停止当前版本(V1)，另部署一套新版本(V2)，不同版本进行共存；
- 灰度发布中，经常设置权重进行分流；比如95%持续老版本(V1)，5%
尝试新版本(V2)；
- 经常与 A/B 测试一起使用，用于测试选择多种方案。

# 第三节：Git版本控制器

![Untitled](Git-Git%20Lab-Jenkins%2046657a7a4e974cc1b9ae9a6039d8b09c/Untitled%208.png)

## 1.概述

持续集成第一步就是开发人员将代码进行提交并推送到代码仓库，比如
**git、svn**。

## 2.版本控制器种类

版本控制器种类分为**分布式**版本控制器和**集中式**版本控制器。

![Untitled](Git-Git%20Lab-Jenkins%2046657a7a4e974cc1b9ae9a6039d8b09c/Untitled%209.png)

分布式版本控制器是不需要“中央服务器”的，每个人的电脑上都是一个
完整的版本库，这样在工作时就不需要联网了，因为版本库就在自己的
电脑上，在多人协作时，只需要把各自的修改推送给对方，就可以互相
看到对方的修改了。（**GIT**）

**SVN：集中式版本控制器**

CVS和SVN都是集中式的版本控制系统，集中式版本控制系统的版本库
是集中存放在中央服务器的，但是因为工作使用的是自己的电脑，所以
在工作之前需要从中央服务器获取最新的代码，并且在工作完成后需要
再上传到中央服务器

### 3.Git特别

Git优点：

- 适合分布式开发，强调个体；
- 公共服务器压力和数据量都不会太大；
- 速度快、灵活；
- 离线工作

GIt缺点：

- 代码保密性差，一旦开发者把整个库克隆下来就可以完全公开所有代码和版本信息；
- 权限控制不友好；如果需要对开发者限制各种权限的建议使用 SVN。

## 4.Git命令学习

| git config —global http://user.name “dingjia” | 配置Git用户 |
| --- | --- |
| git config - -global http://user.email “XX@email.com” | 配置GIT用户邮箱 |
| git config - -global color.ui true | 配置高亮颜色显示 |
| git config -l | 查看配置信息 |
| git init | 初始化工作目录 |
| tree .git/ | 以树状形式查看目录列表 |
| git add status | 查看工作区文件状态 |
| git add 文件名 | 提交文件到暂存区（所有的项目文件都需要经过暂存区才可以被提交到git仓库中） |
| git rm - -cache 文件名 | 将文件从暂存区删除 |
| git rm -f 文件名 | 同时删除暂存区文件与源文件 |
| git commit -m “提交说明” #说明自定义且支持中文 | 提交文件到本地仓库（每一次的commit代码提交相当于虚拟机的一次快照功能。随后可恢复到任意位置。） |
| git log | 查看仓库中历史提交信息 |
| git log - -oneline | 简要查看历史提交信息提示：如没有任何信息，表示没有差异。 |
| git mv a.txt b.txt | 如果需要修改文件名字，可直接使用git命令改名，这样git可以识别到变化： |
| git diff | 比对工作区与暂存区文件差异：提示：如没有任何信息，表示没有差异。 |
| git commit -am “提交信息” | 如果文件已经被仓库管理，再次更改此文件，可直接通过"-am"直接提交到
仓库中。 |
|  |  |

# Git提交流程

## 配置Git

安装Git后，需要使用您的个人设置进行配置，包括您的姓名、电子邮件地址和默认编辑器。

```bash
#使用以下命令设置您的名字和电子邮件
git config --global user.name "Dingjia"
git config --global user.email "gfei8654@gmail.com"
```

## 创建Git仓库

现在，在您的计算机上配置Git，可以初始化一个文件夹作为你的本地Git仓库

```bash
#要创建Git仓库，请导航到要存储文件的中并运行一下命令：
git init
```

## 添加和提交更改

```bash
#创建一个文件夹
echo "Hello,Git!" > hello.txt
#将文件添加到暂存区
git add hello.txt
#将文件提交到本地仓库
git commit -m "Add hello.txt"
```

## 推送更改到远程仓库

最后，您可以将更改推送到远程存储库（如GitHub），以便其他人可以与您一起合作编写代码。要做到这一点，您需要在GitHub上创建一个新存储库，然后运行以下命令：

```bash
git remote add origin https://你的仓库地址.git
git push -u origin master
```

这将添加一个新的远程存储库到您的本地Git存储库，并将更改推送到“master”分支。

### remote和pull命令

git remote是Git命令，作用是管理远程仓库，包括添加、删除、查看远程仓库等操作。它主要用于与远程仓库进行交互，例如推送本地代码到远程仓库，或者从远程仓库拉取代码到本地。

与之相比，git pull是一个常用的Git命令，它的功能是从远程仓库拉取代码并合并到本地分支。在执行git pull命令时，Git会自动执行git fetch操作（从远程仓库获取最新版本的代码），然后执行git merge操作（将获取的代码合并到当前工作分支）。

因此，二者的主要区别在于git pull自动完成了git fetch和git merge的操作，而git remote则提供了更丰富的远程仓库管理功能。

#git remote命令详解：

```bash
#列出本地工作区的远程仓库
git remote -v  （blue和red是远程仓库的别名）
#blue    https://gitee.com/operations-related.git (fetch)
#blue    https://gitee.com/operations-related.git (push)
#red     https://gitee.com/operations-related.git (fetch)
#red     https://gitee.com/operations-related.git (push)
#添加远程仓库命令格式：
git remote  add 远程仓库别名 远程仓库地址
git remote add origin https://github.com/user/repo.git
#删除现有远程仓库
git remote remove red(仓库别名)
```

#git pull命令详解

```bash
#命令格式
git pull <remote> <branch>
#其中，<remote>是远程仓库的名称，<branch>是要拉取的分支名称。如果省略了<branch>参数，
#那么默认会拉取并尝试合并远程仓库的master分支。

#如果你想要从名为origin的远程仓库的master分支获取最新的更改，你可以执行以下命令：
git pull --rebase <remote> <branch>

--这将会将你的本地更改移到一个临时分支上，然后应用远程仓库的更改，最后再将你的本地更改应用回来。这可以保持你的提交历史整洁。
```

**Git强制推送本地分支到远程仓库**

在日常开发中，经常会有需要取消某个版本的提交，或者某些其他操作导致远程分支错误的情况，这时候因为远程分支比本地分支版本更新，会导致提交失败，这时就不得不用本地强制覆盖远程分支了。

在 Git 中,可以通过 git push 命令将本地代码推送到远程仓库，需要强制覆盖时，可在命令后追加一个`-f`参数。

命令格式为:

git push <远程名称> <本地分支名>:<远程分支名>

例如,推送本地的 master 分支到远程的 master 分支,强制覆盖远程代码:

git push origin master:master -f

这里的 -f 选项代表强制(force)推送。

这个命令会将本地的 master 分支推送到远程,并覆盖远程仓库中对应的 master 分支。

## Git工作流程

自己创建编写的文件是工作区，在此编写代码，这个文件会上传到暂存
区，紧接着会上传到本地仓库，也是可以随时取消。最后是可以把代码上
传到远程仓库，分享供大家访问且使用。

![Untitled](Git-Git%20Lab-Jenkins%2046657a7a4e974cc1b9ae9a6039d8b09c/Untitled%2010.png)

****克隆远程仓库****

要将远程仓库复制到本地计算机（甚至是全新的本地仓库），请使用 `git clone` 命令。例如：

```bash
$ git clone https://github.com/username/repository.git
```

****推送更改至远程仓库****

在本地仓库中进行更改后，使用 `git push` 命令将更改推送到远程仓库。例如：

```bash
$ git push origin master
```

****拉取更改****

使用 `git pull` 命令从远程仓库获取最新更改并将其合并到本地仓库。例如：

```bash
$ git pull origin master
```

# Git文件比对

文件比对可以显示工作区文件和暂存区文件的差异、暂存区文件和仓库文件之间的差异

格式：git diff

```bash
#对比工作区与暂存区文件差异：
git diff
#提示：如没有任何信息，表示没有差异

#比对暂存区与本地仓库文件差异：
git diff --cached

*如果文件已经被仓库管理，再次更改此文件，可直接通过“-am”直接提交到仓库中。
git commit -am '第N次提交：修改**内容'
```

上述是当开发人员对项目文件内容更新后，如何进行比对，如何再次提交到仓库

## Git查看历史记录

在使用Git提交若干更新之后，又或者克隆了某个项目，想回顾下提交历史，我们可以使用下边的命令查看：

```bash
#命令格式：
git log #详细的历史提交记录（上下健查看）、q（退出）

git log --pretty=oneline #会以一个漂亮的格式显示（每条日志只显示一行）

git log --oneline        #相对于第二种方式的哈希值指挥显示一部分

[root@git date]# git log
commit 提交的哈希值
Author: 作者信息
Date: 提交时间
提交描述
```

## Git HEAD概念

HEAD是当前分支引用的**指针**，它总是指向某次commit，默认最新一次的commit。

通常可以把HEAD看做你的上一次提交的快照，当然HEAD的指向是可以改变的，比如你提交了commit，切换了仓库，分支，或者回滚了版本，切换tag等

查看指针：在查看历史记录上通过 —decorate查看

- 格式1：git log - -decorate
- 格式2：git log - -pretty=oneline - -decorate
- 格式2：git log - -oneline - -decorate

## Git版本恢复

在实现多人合作程序开发的过程中，我们有时会出现错误提交的情况，此
时我们希望能撤销提交操作，让程序回到提交前的样子，这时候就可以通
过版本回退（reset）来实现。

- 适用场景：如果想恢复到之前某个提交的版本，且那个版本之后提交的版本我们都不要了，就可以使用git reset - -hard 哈希值

```bash
#恢复到第一个版本
 git reset --hard 1fd81198
#查看当前HEAD指向的版本
git log --oneline --decorate
#查看历史提交信息（会发现历史提交过的信息已消失，只保留了当前版本记
录）
git log
#查看历史提交的所有信息：reflog
[root@git date]# git reflog
#再次回滚至最后一次的提交版本
[root@git date]# git reset --hard 4dacc84
```

## Git分支管理

- 使用git的每次提交，Git都会自己把它们串成一条时间线，这条时间线就是一个分支。
- 如果没有新建分支，那么只有一条时间线，即只有一个分支，在Git里，这个分支叫主分支即“**master**”分支
- 通过分支，您可以同时拥有不同版本的存储库。您可以在存储库中创建master分支以外的分支。
- 您可以使用分支一次拥有项目的不同版本。当您想要在不更改主要代码源的情况下向项目添加新功能。
- 在合并主分支之前，在不同分支上完成的工作不会显示在主分支上，您可以使用分支进行试验和编辑，然后再提交到master上。

![Untitled](Git-Git%20Lab-Jenkins%2046657a7a4e974cc1b9ae9a6039d8b09c/Untitled%2011.png)

Git分支常用管理命令：

| 命令 | 功能 |
| --- | --- |
| git branch | 查看分支 |
| git branch 分支名称 | 创建分支 |
| git checkout 分支名称 | 切换分支 |
| git branch -d 分支名称 | 删除分支 |

提示：当你创建完分支并切换时，新分支已经复制了一套master分支所有内容，此时开发人员可以以在其他分支上进行代码提交并进行测试，且并不会影响到mater主干代码

## Git分支合并

使用git merge命令可以将指定分支的文件合并到当前分支。

合并testing分支（提示：在master分支执行合并操作）

```bash
#创建并切换分支，更新提交历史记录
git branch testing
git checkout testing
echo "这是一个新的功能组件" > t1.txt
git add t1.txt
git commit -m "提交一个新的文件"
#查看历史提交记录
git log --pretty=oneline

#切换分支
git branch master
#查看历史提交记录，是否发生改变
git log --oneline
#合并新分支
git merge testing

#查看历史提交记录，查看合并是否成功
git log
```

### Git合并冲突问题

当两个分支修改同一文件的相同部分时，可能会产生冲突。要解决冲突，您必须手动编辑文件并选择要保留的更改。编辑后，使用`git add`标记修改的文件为已解决，然后使用`git commit`提交合并的更改。

试验：a：创建testing1、testing2分支

```bash
git branch testing1
git branch testing2

git branch
#* master
	 testing1
	 testing2

##切换分支testing1分支向t1.txt文件写入内容并提交至仓库
git checkout testing1
echo "testing1 branch" >> t1.txt

git add t1.txt
git commit -m 'add testing1 branch'

##切换到testing2分支向t1.txt文件写入内容并提交至仓库
git checkout testing2
echo "testing2 branch" >> t1.txt

git add t1.txt
git commit -m 'add testing2 branch'
#提示：此时testing1与testing2分支下的t1.txt文件内容不一致。

##切换到master分支进行合并
git checkout master
git merge testing1
git merge testing2
#自动合并 t1.txt
#冲突（内容）：合并冲突 t1.txt
#自动合并失败，修正冲突然后提交修正的结果。

##用VIM解决冲突后手动提交至仓库
vim t1.txt
hello
<<<<<<< HEAD #删除符号行
testing1 branch
======= #删除符号行
testing2 branch
>>>>>>> testing2 #删除符号行

##重新提交至仓库
git add t1.txt
git commit -m 'merge testing1 testing2'

##查看结果
git log --pretty=oneline --decorate
0428c8e (HEAD, master) merge testing1 testing2
```

## Git标签

- Git标签概述

简单理解：标签是某个版本的别名，因为Git的版本号都是用一串字母数字组成，为了便于管理，Git可以给版本取个别名（也就是打上标签，比如标签的名字叫做V1.0.0） 后期可通过标签进行版本回滚。

Git创建标签

命令格式：git tag

常用选项：

- -a  #指定标签名
- -m  #指定描述信息
- -n  #查看时显示标签描述信息
- -d  #删除标签